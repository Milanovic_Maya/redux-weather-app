# This app is made with tutorial Modern React with Redux.
Base code is from [Stephen Grider GitHub](https://github.com/StephenGrider/ReduxSimpleStarter)

This is simple weather application, based on data from [OpenWeatherMap](https://openweathermap.org)

Interested in learning [Redux](https://www.udemy.com/react-redux/)?

