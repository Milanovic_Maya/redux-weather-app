import React, { Component } from "react";
import { connect } from "react-redux";
import Chart from "../components/chart";

class WeatherList extends Component {
    constructor(props) {
        super(props);
        this.renderWeather = this.renderWeather.bind(this);
    }


    renderWeather(cityData) {
        const { name } = cityData.city;

        const temps = cityData.list.map(weather => weather.main.temp);
        const pressures = cityData.list.map(weather => weather.main.pressure);
        const humidities = cityData.list.map(weather => weather.main.humidity);

        const { lon, lat } = cityData.city.coord;


        return (
            //make Chart component - not container!!
            <tr key={name} className="chart-table-row">
                <td>{name}</td>
                <td><Chart data={temps} color={"orange"} units="Celsius" /></td>
                <td><Chart data={pressures} color={"green"} units="mbar" /></td>
                <td><Chart data={humidities} color={"blue"} units="%" /></td>
            </tr>
        )
    }

    render() {
        return (
            <table className="table table-hover">
                <thead>
                    <tr className="chart-table-row">
                        <th>City</th>
                        <th>Temperature (C)</th>
                        <th>Pressure (mbar)</th>
                        <th>Humidity (%)</th>
                    </tr>

                </thead>
                <tbody>
                    {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        )
    }
}

//same as state.weather property
//{weather:weather}
function mapStateToProps({ weather }) {
    return { weather };
}

export default connect(mapStateToProps)(WeatherList);  