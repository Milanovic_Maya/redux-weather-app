import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchWeather } from "../actions/index";

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = { term: "" }

        //binding context in constructor;
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(event) {
        // this is undefined if not bound in constructor!
        const term = event.target.value;
        this.setState({
            term
        });
    }

    onFormSubmit(event) {
        //prevent user to send form before we send ajax request
        event.preventDefault();
             
        //we need to go and fetch weather data

        //when user submits form, call fetchWeather action creator
        this.props.fetchWeather(this.state.term);

        //clear input
        this.setState({ term: "" });

    }


    render() {
        return (
            //on enter or click button, form is submitted
            <form
                onSubmit={this.onFormSubmit}
                className="input-group search-bar">
                {/*controlled field through component state*/}
                <input
                    placeholder="Get a 5-day forecast"
                    className="form-control"
                    value={this.state.term}
                    onChange={this.onInputChange}
                />
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">
                        Submit
                    </button>
                </span>
            </form>
        );
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchWeather }, dispatch);
}

// null as first argument?? 
// because we don't want to map state to props(there is no mapStateToProps())
export default connect(null, mapDispatchToProps)(SearchBar);