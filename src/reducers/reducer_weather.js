import { FETCH_WEATHER } from "../actions/index";

export default function (state = [], action) {
    //action is returned from middleware redux-promise
    //so it's promise resolved, and contains needed data

    //action.payload.data is added to state - array of cities

    //reducer is pure function! we must not mutate state!!!

    //we return new instance of state (new array=old array+new data)
    //concat() doesn't change existing array, creates new array!
    //or use ES6 rest operator to copy state array

    switch (action.type) {
        case FETCH_WEATHER:
            return [action.payload.data, ...state];
    }

    return state;
}