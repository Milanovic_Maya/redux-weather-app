import React from "react";
import { Sparklines, SparklinesLine, SparklinesReferenceLine } from "react-sparklines";


function average(data) {
    return Math.floor((data.reduce((acc, num) => acc + num)) / data.length);
}

export default ({ data, color, units }) => {

    return (
        <div>
            <Sparklines height={120} width={180} data={data}>
                <SparklinesLine color={color}></SparklinesLine>
                {/* adding reference line for average value */}
                <SparklinesReferenceLine type="avg" />
            </Sparklines>
            <div>{average(data)} {units}</div>
        </div>);
}; 