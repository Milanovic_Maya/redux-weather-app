import axios from "axios";

const API_KEY = "15014406e89b07d2ac4f9571bbf1c2c3";
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}&units=metric`;

//to minimize typo errors-save type value in variable
export const FETCH_WEATHER = "FETCH_WEATHER";




//action creator for making ajax request
export function fetchWeather(city) {
    //make url
    const url = `${ROOT_URL}&q=${city},rs`;

    //make request-with axios (npm install --save axios)
    //library for making ajax requests

    const request = axios.get(url);
    //this returns promise
    //request is promise


    //pass as payload

    //action creator always return action(object)
    return {
        //always has property type
        type: FETCH_WEATHER,
        //payload is promise pending
        payload: request
    }
}